trigger MaintenanceRequest on Case (before update, after update)
{
    // call MaintenanceRequestHelper.updateWorkOrders
	if (Trigger.isAfter && Trigger.isUpdate)
	{
		Map<Id, Case> closedMaintenanceRequests = new Map<Id, Case>();
		for (Case caseItem : Trigger.New)
		{
			if ((caseItem.Type == MaintenanceRequestHelper.MAINTENANCE_REQUEST_TYPE_REPAIR || caseItem.Type == MaintenanceRequestHelper.MAINTENANCE_REQUEST_TYPE_ROUTINE)
					&& caseItem.Status == MaintenanceRequestHelper.MAINTENANCE_REQUEST_STATUS_CLOSED && Trigger.oldMap.get(caseItem.Id).Status != caseItem.Status)
			{
				closedMaintenanceRequests.put(caseItem.Id, caseItem);
			}
		}
		MaintenanceRequestHelper.updateWorkOrders(closedMaintenanceRequests);
	}
}