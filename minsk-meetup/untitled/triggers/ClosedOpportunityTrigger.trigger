trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) 
{
    List<Task> tasks = new List<Task>();
    for (Opportunity opp : Trigger.new)
    {
        Task task1 = new Task();
        if (opp.StageName == 'Closed Won')
        {
            task1.Subject = 'Follow Up Test Task';
            task1.WhatId = opp.Id;
        }
        tasks.add(task1);
    }
    insert tasks;
}