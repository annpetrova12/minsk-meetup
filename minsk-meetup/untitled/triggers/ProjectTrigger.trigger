trigger ProjectTrigger on Project__c (after update) {
    //Call the Billing Service callout logic here
	if (Trigger.isAfter)
	{
		Set<Id> billableProjects = new Set<Id>();
		for (Project__c project : Trigger.New)
		{
			if (project.Status__c == 'Billable' && project.Status__c != Trigger.oldMap.get(project.Id).Status__c)
			{
				BillingCalloutService.callBillingService(project.ProjectRef__c, project.Billable_Amount__c, project.Id);
			}
		}
	}
}