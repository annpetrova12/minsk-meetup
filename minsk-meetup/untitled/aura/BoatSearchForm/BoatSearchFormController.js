({
	doInit: function(component, event, handler) {
		console.log('inside do init ');

		var getBoatTypes = component.get("c.getBoatTypes");

		getBoatTypes.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS")
			{
				component.set("v.boatTypes", response.getReturnValue());
			}
			else
			{
				console.log(response);
			}
		});

		$A.enqueueAction(getBoatTypes);

		component.set('v.isCreateable', $A.get('e.force:createRecord'));
	},

	onFormSubmit: function (component, event, helper) {
		var formSubmit = component.getEvent("formSubmit");
		var boatTypeId = component.get("v.selectedType");
		console.log('boat search form, method onFormSubmit');
		console.log('boatTypeId = ' + boatTypeId);
		formSubmit.setParams({"formData" : {"boatTypeId" : boatTypeId}
		});
		formSubmit.fire();
	},

	handleNewBoat: function(component, event, helper) {
		var createNewBoat = $A.get("e.force:createRecord");
		var boatTypeId = component.get("v.selectedType");
		createNewBoat.setParams({
				"entityApiName": "Boat__c"
		});
		if (boatTypeId != ""){
			createNewBoat.setParams({
					"defaultFieldValues": {'BoatType__c': boatTypeId}
			});
		}
		createNewBoat.fire()
	},
})