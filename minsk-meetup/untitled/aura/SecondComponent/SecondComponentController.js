({
	getLead : function(component, event, helper) {
		var getRecordAction = component.get("c.getLeadName");
        
         getRecordAction.setParams({
            "recordId" : (component.get("v.leadId"))
        });
        
        getRecordAction.setCallback(this, function(response){
            if(component.isValid() && response.getState() === "SUCCESS"){
                component.set("v.newLeadRecord",response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getRecordAction);
	}
})