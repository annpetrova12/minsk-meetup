({
	confirmationCancel : function(component, event, helper) {
		component.set("v.showConfirmation", false);
	},
    confirmationDelete : function(component, event, helper) {
		var action = component.get("c.deleteRecord"); 
        action.setParams({
            "record" : component.get("v.externalItem")
        });
        $A.enqueueAction(action);
		component.set("v.showConfirmation", false);
        component.getEvent("ExternalListItemRefreshTable").fire();
	}
})