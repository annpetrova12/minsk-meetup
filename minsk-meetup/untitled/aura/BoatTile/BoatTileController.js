({
	onBoatClick: function (component, event, helper) {
		var boat = component.get("v.boat");
		var boatSelect = component.getEvent("BoatSelect");
		boatSelect.setParams({
			"boatId" : boat.Id
		})
		boatSelect.fire();

		var boatSelected = $A.get('e.c:BoatSelected');
		boatSelected.setParams({
			"boat" : boat
		});
		boatSelected.fire();
		console.log('boatTile js - fire boat selected');

		var plotMapMaker = $A.get("e.c:PlotMapMarker");
		plotMapMaker.setParams({
			"lat" : boat.Geolocation__Latitude__s,
			"long" : boat.Geolocation__Longitude__s,
			"label" : boat.Name,
			"sObjectId" : boat.Id
		});
		plotMapMaker.fire();
	}
})