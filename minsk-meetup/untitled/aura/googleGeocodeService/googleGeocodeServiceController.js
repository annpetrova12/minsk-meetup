({
	init: function (component, event, helper) {
		var getConnectionSettingsAction = component.get("c.getConnectionSettings");
		getConnectionSettingsAction.setParams({
			"connectionSettingsDeveloperName": component.get("v.connectionSettingsDeveloperName")
		});
		getConnectionSettingsAction.setCallback(this, function (response) {
			component.set("v.connectionSettings", response.getReturnValue());
		});
		$A.enqueueAction(getConnectionSettingsAction);
	},

	runAddressSearch: function (component, event, helper) {
		console.log("inside runAddressSearch");

		var searchAddressAction = component.get("c.searchAddress");
		searchAddressAction.setParams({
			"searchLine": component.get("v.searchLine"),
			"connectionSettings": component.get("v.connectionSettings")
		});
		searchAddressAction.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS") {
				var rawSearchResult = JSON.parse(response.getReturnValue());
				component.set("v.rawSearchResult", rawSearchResult.results);
				console.log("rawSearchResult: " + JSON.stringify(rawSearchResult.results));

				var methodArguments = event.getParam("arguments");
				var callback = $A.util.isEmpty(methodArguments) ? null : methodArguments.callback;

				if (!$A.util.isEmpty(methodArguments)) {
					callback(rawSearchResult.results);
				}
			} else {
				helper.showToast({
					"mode": "dismissible",
					"message": response.getError()[0].message,
					"type": "error"
				});
			}
		});
		$A.enqueueAction(searchAddressAction);
	},
})