({
    showToast : function(toastParameters) {
        var showToastEvent = $A.get("e.force:showToast");
        showToastEvent.setParams(toastParameters);
        showToastEvent.fire();
    },
})