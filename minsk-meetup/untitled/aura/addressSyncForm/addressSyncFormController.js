({
	init : function(component, event, helper)
	{
		var getRecord = component.get("c.getRecord");
		getRecord.setParams({
			"recordId" : component.get("v.recordId"),
		});
		getRecord.setCallback(this, function(response){
			if (response.getState() === "SUCCESS"){
				component.set("v.record", response.getReturnValue());
			}
			else {
				console.log("Error: " + response.getError()[0].message);
			}
		});
		$A.enqueueAction(getRecord);
	},

	goBack: function(component, event, helper)
	{
		component.set("v.result", null);
		component.set("v.searchLine", null)
	},

	onAddressSelected : function(component, event, helper)
	{
		component.set("v.result", event.getParam("recordByEvent"));
		var address = helper.parseSearchResult(component.get("v.result"));
		component.set("v.fullAddress", address.fullAddress);
		component.set("v.city", address.city);
		component.set("v.country", address.country);
		component.set("v.state", address.state);
		component.set("v.street", address.street);
		component.set("v.postalCode", address.zipCode);
		component.set("v.latitude", address.geolocation.lat);
		component.set("v.longitude", address.geolocation.lng);
	},

	onSync : function(component, event, helper)
	{
		var record = component.get("v.record");
		record[component.get("v.fullAddressApiName")] = component.get("v.fullAddress");
		record[component.get("v.countryApiName")] = component.get("v.country");
		record[component.get("v.stateApiName")] = component.get("v.state");
		record[component.get("v.streetApiName")] = component.get("v.street");
		record[component.get("v.cityApiName")] = component.get("v.city");
		record[component.get("v.postalCodeApiName")] = component.get("v.postalCode");
		record[component.get("v.latitudeApiName")] = component.get("v.latitude");
		record[component.get("v.longitudeApiName")] = component.get("v.longitude");

		var saveAddress = component.get("c.saveAddress");
		saveAddress.setParams({
			"record" : record
		});
		saveAddress.setCallback(this, function(response){
			if (response.getState() === "SUCCESS") {
				var resultToast = $A.get("e.force:showToast");
				resultToast.setParams({
					"title" : "Saved",
					"message" : "Address updated successfully!"
				}).fire;
			}
			else {
				console.log("Error: " + response.getError()[0].message);
			}
		});
		$A.enqueueAction(saveAddress);
		$A.get('e.force:refreshView').fire();
	}
})