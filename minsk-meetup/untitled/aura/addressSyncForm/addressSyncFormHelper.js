({
	parseSearchResult : function(rawSuggestion) {
		var suggestedAddress = {
			"fullAddress" : null,
			"country" : null,
			"state" : null,
			"street" : null,
			"city" : null,
			"zipCode" : null,
			"geolocation" : null
		};

		suggestedAddress.fullAddress = rawSuggestion.formatted_address;
		suggestedAddress.geolocation = rawSuggestion.geometry.location;

		var addressComponents = rawSuggestion.address_components;

		for (var addressComponent of addressComponents) {
			if (addressComponent.types.includes("street_number")) {
				suggestedAddress.street = addressComponent.long_name;
			} else if (addressComponent.types.includes("route")) {
				suggestedAddress.street += " " + addressComponent.long_name;
			}else if (addressComponent.types.includes("country")) {
				suggestedAddress.country = addressComponent.long_name;
			} else if (addressComponent.types.includes("administrative_area_level_1")) {
				suggestedAddress.state = addressComponent.long_name;
			} else if (addressComponent.types.includes("locality")) {
				suggestedAddress.city = addressComponent.long_name;
			} else if (addressComponent.types.includes("postal_code")) {
				suggestedAddress.zipCode = addressComponent.long_name;
			}
		}

		return suggestedAddress;
	},
})