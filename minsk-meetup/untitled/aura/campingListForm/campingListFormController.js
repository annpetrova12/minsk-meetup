({
	clickCreateItem : function(component, event, helper) {
		var validTrue = component.find("newItemId").reduce(
            function (validSoFar, inputComponent)
            {
                 inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputComponent.get("v.validity").valid;
            }, true);
        
        if (validTrue)
        {          
            campingListFormHelper.createItem(component);          
        }
	}
})