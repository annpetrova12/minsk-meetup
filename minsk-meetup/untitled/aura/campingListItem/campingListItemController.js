({
	packItem : function(component, event, helper) {
		var btnSource = event.getSource();
        var value = btnSource.get("v.label");
        component.set("v.item.Packed__c", true);
        btnSource.Disabled = true;
	}
})