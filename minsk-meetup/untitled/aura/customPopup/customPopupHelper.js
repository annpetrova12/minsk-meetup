({
	doSearch: function (component, event) {
		var geocodeService = component.find("googleGeocode");
		geocodeService.searchSuggestions(function (result) {
			var resultLines = [];
			for (var i = 0; i < result.length; i ++)
			{
				resultLines.push(result[i]);
				console.log('result ' + result[i].formatted_address);
			}
			component.set("v.searchResults", resultLines);
		});
	}
})