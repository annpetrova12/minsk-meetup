({
	doInit : function(component,event,helper){
		var selectedRecord = component.get("v.selectedRecord");
		if (!$A.util.isEmpty(selectedRecord)) {
			component.set("v.pillContent", selectedRecord["formatted_address"]);
		}
	},

	onfocus : function(component,event,helper){
		$A.util.addClass(component.find("mySpinner"), "slds-show");
		var searchRes = component.find("searchRes");
		$A.util.addClass(searchRes, 'slds-is-open');
		$A.util.removeClass(searchRes, 'slds-is-close');

		helper.doSearch(component, event);
	},

	onblur : function(component,event,helper){
		component.set("v.searchResults", null );

		var searchRes = component.find("searchRes");
		$A.util.addClass(searchRes, 'slds-is-close');
		$A.util.removeClass(searchRes, 'slds-is-open');
	},

	keyPressHandler : function(component, event, helper) {
		var inputKey = component.get("v.searchKeyWord");
		if( inputKey.length > 0 ){
			var searchRes = component.find("searchRes");
			$A.util.addClass(searchRes, 'slds-is-open');
			$A.util.removeClass(searchRes, 'slds-is-close');
			helper.doSearch(component,event);
		}
		else {
			component.set("v.searchResults", null );
			var searchRes = component.find("searchRes");
			$A.util.addClass(searchRes, 'slds-is-close');
			$A.util.removeClass(searchRes, 'slds-is-open');
		}
	},

	clear :function(component,event,heplper){
		var pillTarget = component.find("lookup-pill");
		var lookUpTarget = component.find("input");

		$A.util.addClass(pillTarget, 'slds-hide');
		$A.util.removeClass(pillTarget, 'slds-show');

		$A.util.addClass(lookUpTarget, 'slds-show');
		$A.util.removeClass(lookUpTarget, 'slds-hide');

		component.set("v.searchKeyWord",null);
		component.set("v.searchResults", null );
		component.set("v.selectedRecord", null );
		component.set("v.pillContent", '' );

	},

	handleSelect : function(component, event, helper) {
		var selectedRecord = event.getParam("recordByEvent");
		var pillContent = event.getParam("pillContent");
		component.set("v.selectedRecord" , selectedRecord);
		component.set("v.pillContent" , pillContent);
		console.log("pillcontent " + pillContent ) ;

		var pillCmp = component.find("lookup-pill");
		$A.util.addClass(pillCmp, 'slds-show');
		$A.util.removeClass(pillCmp, 'slds-hide');

		var searchRes = component.find("searchRes");
		$A.util.addClass(searchRes, 'slds-is-close');
		$A.util.removeClass(searchRes, 'slds-is-open');

		var lookUpTarget = component.find("input");
		$A.util.addClass(lookUpTarget, 'slds-hide');
		$A.util.removeClass(lookUpTarget, 'slds-show');
	},


	handleRecordChange : function(component, event, helper) {
		var record = component.get("v.selectedRecord");
		if ($A.util.isEmpty(record)) {
			component.set("v.pillContent", '');
		} else {
			component.set("v.pillContent", record["formatted_address"]);
		}

	},
})