({
	createItem : function(component, event, handler) {
		var items = component.get("v.items");
        var newItem = event.getParams("item");     
        var saveItem = component.get("c.saveItem");
        
        saveItem.setParams({
            "item" : newItem
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                items.push(JSON.parse(JSON.stringify(newItem)));
                component.set("v.items", items);
               /* component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c',
                                           'Name': '',
                                           'Quantity__c': 0,
                                           'Price__c': 0,
                                           'Packed__c': false }); */
            }
        });
        
        $A.enqueueAction(saveItem);    
	}
})