({
    doInit : function(component, event, handler)
    {
        //execute apex controller method
       var getItems = component.get("c.getItems");
        getItems.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                component.set("v.items", component.getReturnValue());
            }
            else
            {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be executed
        $A.enqueueAction(getItems);
    },
    
	handleAddItem : function(component, event, helper) {
		//campingListHelper.createItem(component, event, handler);
		var items = component.get("v.items");
        var newItem = event.getParams("item");     
        var saveItem = component.get("c.saveItem");
        
        saveItem.setParams({
            "item" : newItem
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                items.push(JSON.parse(JSON.stringify(newItem)));
                component.set("v.items", items);
               /* component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c',
                                           'Name': '',
                                           'Quantity__c': 0,
                                           'Price__c': 0,
                                           'Packed__c': false }); */
            }
        });
        
        $A.enqueueAction(saveItem);    
	}
})