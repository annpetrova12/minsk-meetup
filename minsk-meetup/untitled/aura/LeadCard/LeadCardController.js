({
	rerenderView : function(component, event) {
		//console.log(event.getParam("leadId"));
		
		var container = component.find("container");
        
        $A.createComponent("c:SecondComponent",{"leadId" : event.getParam("leadId")}, function(newComponent,status,errorMessage){
            if(status === "SUCCESS"){
                container.set("v.body", newComponent);
            }
        });
	}
})