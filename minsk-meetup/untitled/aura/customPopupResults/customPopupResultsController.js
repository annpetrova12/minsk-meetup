({
	doInit : function(component, event, helper){
		var fieldToDisplay = "v.record." + component.get("v.fieldToDisplay");
		var pillContent = component.get(fieldToDisplay);
		component.set("v.pillContent", pillContent);
	},

	selectRecord : function(component, event, helper){
		var selectedRecord = component.get("v.record");
		var selectRecordEvent = component.getEvent("recordSelected");
		selectRecordEvent.setParams({
			"recordByEvent" : selectedRecord,
			"pillContent"   : component.get("v.pillContent")
		}).fire();
	},
})