({
	loadExternalItems : function(component) {
        var self = this;
        this.toggleSpinner(component, true);
        var action = component.get("c.getExternalItems");
        action.setParams({
            "amountOfVisible" : component.get("v.pageSize"),
            "numberOfPage" : component.get("v.page"),
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if (component && component.isValid() && state === "SUCCESS"){ 
                component.set("v.externalListItem",response.getReturnValue());
            } 
        	self.toggleSpinner(component, false);
        });
        $A.enqueueAction(action);
     	this.checkAccessibility(component);
    },
    setAttributes : function (component) {
        var self = this;
        this.toggleSpinner(component, true);
    	var action = component.get("c.getAmountOfExternalItems");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response) { 
            if (component && component.isValid() && state === "SUCCESS"){ 
                var state = response.getState();
                component.set("v.totalItems", response.getReturnValue());
                component.set("v.pageAmount", Math.ceil(response.getReturnValue()/ component.get("v.pageSize")));
                self.toggleSpinner(component, false);
            }
        });
        $A.enqueueAction(action);
    },
    createRecord : function (component) {
  		var createRecordEvent = $A.get("e.force:createRecord");
  		createRecordEvent.setParams({
  	      	"entityApiName": "External_Item__c",
            "defaultFieldValues": {
            	'ParentID__c' : component.get("v.recordId")
      		}
   		});
   		createRecordEvent.fire();
	},
    checkAccessibility: function(component){
        var self = this;
        this.toggleSpinner(component, true);
        var isEditable = component.get("c.isUpdateable");
        isEditable.setParams({
            "recordId" : component.get("v.recordId")
        });
        isEditable.setCallback(this, function(response) {
            var state = response.getState();
            if (component && component.isValid() && state === "SUCCESS"){ 
                component.set("v.showActions", response.getReturnValue());
                self.toggleSpinner(component, false);
            }
        });
       	$A.enqueueAction(isEditable);
    },
    toggleSpinner: function(component, state){
        var spinner = component.find("spinner");
        if (state) {
            $A.util.removeClass(spinner, "slds-hide");
        }
        else {
            $A.util.addClass(spinner, "slds-hide");
        }
    }    
})