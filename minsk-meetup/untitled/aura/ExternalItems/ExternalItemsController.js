({
	doInit : function(component, event, helper) { 
 		helper.setAttributes(component);
        helper.loadExternalItems(component);
	},
    createNewRecord : function(component, event, helper) {
        helper.createRecord(component);
    },
    previousPage : function(component, event, helper) {
        var page = component.get("v.page");
        component.set("v.page", page - 1);
        helper.loadExternalItems(component);
    },
    nextPage : function(component, event, helper) {
        var page = component.get("v.page");
        component.set("v.page", page + 1);
        helper.loadExternalItems(component);
    }
})