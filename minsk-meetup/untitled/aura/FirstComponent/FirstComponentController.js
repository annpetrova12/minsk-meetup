({
	saveRecord : function(component) {
		var saveRecordAction = component.get("c.saveLeadRecord");
        
        saveRecordAction.setParams({
            "leadRecordJSON" : JSON.stringify(component.get("v.newLeadRecord")) 
        });
        
        saveRecordAction.setCallback(this, function(response){
            if(component.isValid() && response.getState() === "SUCCESS"){
                
             //   console.log(response.getReturnValue());
                
                var leadCreatedEvent = component.getEvent("leadCreated");
                leadCreatedEvent.setParams({"leadId": response.getReturnValue()}).fire();
            }
        });
        
        $A.enqueueAction(saveRecordAction);
	}
})