({
	doInit: function (component, event, helper) {
		helper.onInit(component, event, helper);
	},

	onUserInfoClick: function (component, event, helper) {
		var userId = event.currentTarget.getAttribute("data-userid");
		var navigation = $A.get("e.force:navigateToSObject");
		navigation.setParams({
			"recordId" : userId
		});
		navigation.fire();
	},

	refresh: function (component, event, helper) {
		this.doInit(component, event, helper);
	}
})