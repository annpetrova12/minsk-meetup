({    
    handleEditAction : function(component, event, helper) {         
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.externalItem.Id")
        });
        editRecordEvent.fire();
    },
    handleDeleteAction : function(component, event, helper) { 
        component.set("v.showConfirmation", true); 
    },
    openMenu : function(component, event, helper) { 
        if (component.get("v.showMenu") == true) {
            component.set("v.showMenu", false);
        }
        else {
        	component.set("v.showMenu", true);
        }
    }
})