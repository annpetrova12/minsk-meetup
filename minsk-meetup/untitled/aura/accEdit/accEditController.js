({
	handleSaveAccount : function(component, event, helper) {
		var recordEditor = component.find("recordEditor");
        recordEditor.saveRecord($A.getCallback(function(saveResult) {
          /*  if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT")
            {
                console.log("Success");
            }
            else if (saveResult.state === "INCOMPLETE")
            {
                console.log("Incomplete");
            }
            else
            {
                console.log("Unknown problem :(");
            } */
             if (saveResult.state === "ERROR") {
                var errMsg = "";
                // saveResult.error is an array of errors, 
                // so collect all errors into one message
                for (var i = 0; i < saveResult.error.length; i++) {
                    errMsg += saveResult.error[i].message + "\n";
                }
                component.set("v.recordSaveError", errMsg);
            } else {
                component.set("v.recordSaveError", "");
            }
        }));
	},
    
    handleRecordUpdated : function (component, event, handler) {
        var eventParams = event.getParams();
        if (eventParams.changeType = "CHANGED") {
            var changedFields = eventParams.changedFields;
            console.log('Fields that are changed: ' + JSON.stringify(changedFields));
            
            var resultToast = $A.get("e.force:showToast");
            resultToast.setParams({
                "title" : "Saved",
                "message" : "The record was updated."
            });
            
            resultToast.fire();
        } else if(eventParams.changeType === "LOADED") {
            // record is loaded in the cache
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted and removed from the cache
        } else if(eventParams.changeType === "ERROR") {
            console.log('Error: ' + component.get("v.error"));
        }
    }
})