({
	onFormSubmit: function (component, event, helper) {
		console.log('boat main form, method onFormSubmit');
		var formData = event.getParam("formData");
		var boatTypeId = formData.boatTypeId;

		console.log('boat type Id here = ' + boatTypeId);

		var searchResultComponent = component.find("searchResultsId");
		var methodResult = searchResultComponent.search(boatTypeId);
	}
})