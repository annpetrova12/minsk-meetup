({
	init: function (component, event, helper) {
		component.set("v.enableFullDetails", $A.get("e.force:navigateToSObject"));
	},

	onFullDetails : function (component, event, helper) {
		var navigation = $A.get("e.force:navigateToSObject");
		navigation.setParams({
			"recordId" : component.get("v.boat.Id")
		});
		navigation.fire();
	}
})