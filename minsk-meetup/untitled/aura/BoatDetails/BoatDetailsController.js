({
	onBoatSelected: function (component, event, helper) {
		var param = event.getParam("boat");
		component.set("v.id", param.Id);
		component.set("v.boat", param);
		/*var service = component.find("service");
		var boat = component.get("v.boat");
		service.set("v.recordId", boat.Id);
		service.reloadRecord() ;*/
	},

	onBoatReviewAdded: function (component, event, helper) {
		component.find("details").set("v.selectedTabId", "boatreviewtab");
		var boatReviews = component.find("BoatReviews");
		boatReviews.refresh();
	},

	onRecordUpdated: function (component, event, helper) {
		var boat = component.get("v.boat");
		var boatReviews = component.find("BoatReviews");
		if (typeof boatReviews != 'undefined')
		{
			boatReviews.refresh();
		}
	},

	refresh : function (component, event, helper) {
		var action = component.get("");
		action.setCallback(this, function (response) {
			if (response.getState() === "SUCCESS")
			{
				$A.get("e.force:refreshView").fire();
			}
		});
		$A.enqueueAction(action);
	}
})