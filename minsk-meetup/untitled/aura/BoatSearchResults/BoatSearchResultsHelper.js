({
	onSearch: function(component, event, helper) {
		console.log('In Helper');
		console.log('boatTypeId in BSearchResults = ' + component.get("v.boatTypeId"));
		var getBoats = component.get("c.getBoats");
		getBoats.setParams({
			"boatTypeId" : component.get("v.boatTypeId")
		});
		getBoats.setCallback(this, function(response) {
			var state = response.getState();
			if(state === 'SUCCESS') {
				component.set("v.boats", response.getReturnValue());
			}
		});
		$A.enqueueAction(getBoats);
	}
})