public class Accounts extends  fflib_SObjectDomain{
 	public Accounts(List<Account> sObjectList) {
        super(sObjectList);
    }
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new Accounts(sObjectList);
        }
    }

    public override void onBeforeInsert(){
        for(Account acc: (List<Account>) Records){
            acc.Description = 'Domain classes rock!';
        }
    }
    public override void onApplyDefaults(){
        for(Account acc: (List<Account>) Records){
            acc.Description = 'Domain classes rock!';
        }
    }
    public override void onbeforeUpdate(Map<Id,SObject> existingRecords)
    {
        for(Account acc: (List<Account>)Records)
        {
            Account oldVal = (Account)existingRecords.get(acc.Id);
            String s = oldVal.Description;
			acc.AnnualRevenue = (acc.Description).getLevenshteinDistance(s);
        }   
    }
}