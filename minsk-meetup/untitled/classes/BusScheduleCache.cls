public class BusScheduleCache
{
    private Cache.OrgPartition part;
    
    public BusScheduleCache()
    {
        this.part = Cache.Org.getPartition('local.BusSchedule');
    }
    
    public void putSchedule(String busLine, Time[] schedule)
    {
        //stores the passed-in values in the org cache by using the partition class variable (part)
        this.part.put(busLine, schedule);
    }
    
    public Time[] getSchedule(String busLine)
    {
        // returns the schedule for the specified bus line by using the partition class variable (part).
        // handle cache misses. If null is returned for the cached value, 
        // getSchedule() should return the following default schedule as a Time array with two Time objects: 
        // one Time object value of 8am and another of 5pm.
        Time[] timeArray = (Time[]) this.part.get(busline);
        
        if (timeArray != null)
        {       
        	return timeArray;
        }
        else
        {
            timeArray = new Time[]{Time.newInstance(8, 0, 0, 0), Time.newInstance(17, 0, 0, 0)};
            return timeArray;
        }
    }
}