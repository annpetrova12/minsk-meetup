public with sharing class WarehouseCalloutService
{
    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';

    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
	@future(callout=true)
    public static void runWarehouseEquipmentSync()
    {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setMethod('GET');
		request.setEndpoint(WAREHOUSE_URL);
		HttpResponse response = http.send(request);
		List<CalloutHelper.Equipment> equipments = (List<CalloutHelper.Equipment>) JSON.deserialize(response.getBody(), List<CalloutHelper.Equipment>.class);

		System.debug(equipments);
		List<Product2> result = new List<Product2>();
		for (CalloutHelper.Equipment equipment : equipments)
		{
			Product2 product = new Product2();
			product.Name = equipment.name;
			product.Id = equipment.id;
			product.Cost__c = equipment.cost;
			product.Lifespan_Months__c = equipment.lifespan;
			product.Replacement_Part__c = equipment.replacement;
			product.Warehouse_SKU__c = equipment.sku;
			product.Current_Inventory__c  = equipment.quantity;
			product.Maintenance_Cycle__c = equipment.maintenanceperiod;
			result.add(product);
		}

		upsert result;
    }

}