@isTest
public class AccountProcessorTest {
    @isTest
    static void testFuture()
    {
        List<Account> accs = new List<Account>();
        for (Integer i= 0; i< 10; i++)
        {
            accs.add(new Account(Name = 'Garry' +i));
        }
        insert accs;
        accs = [SELECT Id FROM Account limit 200];
        List<Id> ids = new List<Id>();
        for (Account acc : accs)
        {
            ids.add(acc.Id);
        }
        Test.startTest();
        AccountProcessor.countContacts(ids);
        Test.stopTest();
        
    }

}