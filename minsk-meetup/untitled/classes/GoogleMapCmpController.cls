public with sharing class GoogleMapCmpController
{
	@AuraEnabled
	public static SObject getRecord(String recordId, String latitudeApiName, String longitudeApiName)
	{
		String sObjectType = ((Id)recordId).getSObjectType().getDescribe().getName();
		String query = 'SELECT ' + latitudeApiName + ', ' + longitudeApiName +
				+ ' FROM ' + sObjectType +
				+ ' WHERE Id = \'' + recordId + '\'';
		List<SObject> record = Database.query(query);
		if (!record.isEmpty())
		{
			if (record[0].get(latitudeApiName) == null || record[0].get(longitudeApiName) == null) {
				throw new AuraHandledException('Map can\'t be displayed as location field has blank value.');
			}

			return record[0];
		}

		return null;
	}

	@AuraEnabled
	public static String getGeolocation(String recordId, String sObjectName, String geolocationFieldName)
	{
		validateGeolocationField(sObjectName, geolocationFieldName);

		String query = 'SELECT ' + geolocationFieldName +
				+ ' FROM ' + sObjectName +
				+ ' WHERE Id = \'' + recordId + '\'';

		List<SObject> record = Database.query(query);
		if (!record.isEmpty())
		{
			Location coordinates = (Location)record[0].get(geolocationFieldName);
			if (coordinates == null) {
				throw new AuraHandledException('Map can\'t be displayed as location field has blank value.');
			}

			return JSON.serialize(coordinates);
		}

		return null;
	}

	@AuraEnabled
	public static String getGoogleApiKey(String connectionSettingsDeveloperName)
	{
		List<Google_Maps_API_Settings__mdt> connectionSettings = [
			SELECT Google_Maps_API_Key__c
			FROM Google_Maps_API_Settings__mdt
			WHERE DeveloperName = :connectionSettingsDeveloperName
			LIMIT 1
		];

		if (!connectionSettings.isEmpty())
		{
			return connectionSettings[0].Google_Maps_API_Key__c;
		}

		return null;
	}

	public static void validateGeolocationField(String sObjectName, String geolocationFieldName)
	{
		Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
		Map<String, Schema.SObjectField> sObjectsFields = sObjectType.getDescribe().fields.getMap();

		Schema.SObjectField geolocationField = sObjectsFields.get(geolocationFieldName);
		if (geolocationField == null)
		{
			throw new AuraHandledException('Map can\'t be displayed as records of this type don\'t have "' + geolocationFieldName + '" field. Please ask your system administrator to set proper field for this component.');
		}
		if (geolocationField.getDescribe().getType() != Schema.DisplayType.LOCATION)
		{
			throw new AuraHandledException(geolocationFieldName + ' is not a geolocation field. Only geolocation fields can be displayed in this component. Please ask your system administrator to set proper field for this component.');
		}
		if (!geolocationField.getDescribe().isAccessible())
		{
			throw new AuraHandledException('Map can\'t be displayed as you don\'t have access to geolocation field displayed here.');
		}
	}
}