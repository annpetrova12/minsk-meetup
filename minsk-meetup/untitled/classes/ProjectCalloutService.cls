public class ProjectCalloutService
{
    //Complete the implementation for business and callout logic
	@InvocableMethod
	public static void postOpportunityToPMS(List<Id> oppIds)
	{
		if (oppIds.isEmpty())
		{
			return ;
		}

		Opportunity oppToSend = [SELECT Id, Name, Account.Name, Amount, CloseDate FROM Opportunity WHERE Id =: oppIds.get(0) LIMIT 1];
		ServiceTokens__c projectServiceToken = ServiceTokens__c.getValues('ProjectServiceToken');
		String json = JSON.serialize(build(oppToSend));
		System.debug(json);
		System.enqueueJob(new QueueablePMSCall(oppToSend.Id, projectServiceToken.Token__c, json));
    }

	public class QueueablePMSCall implements System.Queueable, Database.AllowsCallouts
	{
		private Id oppId;
		private String token;
		private String json;

		public QueueablePMSCall(Id oppId, String token, String json)
		{
			this.oppId = oppId;
			this.token = token;
			this.json = json;
		}

		public void execute(System.QueueableContext context)
		{
			doPost(this.oppId, this.token, json);
			System.debug('After do post');
		}
	}

	@future(callout=true)
	public static void doPost(Id oppId, String token, String json)
	{
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setHeader('token', token);
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
		request.setEndpoint('callout:ProjectService');
		request.setBody(json);
		HttpResponse response = http.send(request);

		System.debug('In future');
		Opportunity opp = new Opportunity(Id=oppId);
		if(response.getStatusCode() != 201){
			opp.StageName = 'Resubmit Project';
			System.debug('Failure: ' + response.getStatusCode() + ' ' + response.getStatus());
		} else {
			opp.StageName = 'Submitted Project';
			System.debug('Success: ' + response.getStatus());
		}
		update opp;
	}

	private static OpportunityCalloutWrapper build(Opportunity opp)
	{
		OpportunityCalloutWrapper oppCW = new OpportunityCalloutWrapper();
		oppCW.opportunityId = opp.Id;
		oppCW.opportunityName = opp.Name;
		oppCW.accountName = opp.Account.Name;
		oppCW.closeDate = opp.CloseDate;
		oppCW.amount = opp.Amount;

		return oppCW;
	}

	private class OpportunityCalloutWrapper
	{
		private String opportunityId {get; set;}
		private String opportunityName {get; set;}
		private String accountName {get; set;}
		private Date closeDate {get; set;}
		private Decimal amount {get; set;}
	}
}