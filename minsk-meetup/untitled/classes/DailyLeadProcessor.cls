public class DailyLeadProcessor implements Schedulable
{
    public void execute(SchedulableContext ctx) 
    {
        List<Lead> leads = [Select id from lead where leadsource = null limit 200];
        for (lead le : leads)
        {
            le.leadsource = 'Dreamforce';
        }
        update leads;
    }
}