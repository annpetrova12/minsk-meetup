public class AccountsSelector {
    public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<SObjectField> {
			Account.Id,
			Account.Name,
                Account.Description,
                Account.AnnualRevenue
		};
	}
    	public List<Account> selectById(Set<ID> idSet)
    {
        fflib_QueryFactory contactFactory = new fflib_QueryFactory(Account.SObjectType);
		contactFactory.selectFields(new Set<String> { 'Name', 'Description','AnnualRevenue' }).setCondition('Id IN: idSet');
		String query = contactFactory.toSOQL();        
        return (List<Account>) Database.query(query);
    }
	
}