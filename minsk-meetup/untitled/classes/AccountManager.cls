@RestResource(urlMapping='/Accounts/*/contacts')
global class AccountManager 
{
    @HttpGet
    global static Account getAccount()
    {
        RestRequest request = RestCOntext.request;
        String accId = request.requestURI.substring(request.requestURI.lastIndexOf('Accounts/')+9, request.requestURI.indexOf('/contacts'));
        System.debug(accId);
        Account result = [select id, name, (select id, name from contacts) from account where id = :accId limit 1];
        return result;
    }

}