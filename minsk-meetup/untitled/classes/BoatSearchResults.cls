/**
 * Created by anna.petrova on 7/24/2018.
 */
public with sharing class BoatSearchResults
{
	@AuraEnabled
	public static List<Boat__c> getBoats(String boatTypeId)
	{
		List<Boat__c> boats = new List<Boat__c>();
		if(boatTypeId != '')
		{
			boats = [SELECT BoatType__c, Contact__r.Name, Picture__c, Name, Geolocation__Latitude__s, Geolocation__Longitude__s FROM Boat__c
								WHERE BoatType__c = :boatTypeId];
		}
		else
		{
			boats = [SELECT BoatType__c, Contact__r.Name, Picture__c, Name, Geolocation__Latitude__s, Geolocation__Longitude__s FROM Boat__c
								ORDER BY Id];
		}
		return boats;
	}
}