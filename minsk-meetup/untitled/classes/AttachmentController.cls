/**
 * Created by anna.petrova on 8/1/2018.
 */
public class AttachmentController {
	@AuraEnabled
	public static void updatePicturePath(String recId)
	{
		System.debug('00 + ' + recId);
		recId = (recId == null || recId == '') ? 'a0L0K00000FqN3QUAV' : recId;
		//In Lightning Experience, Attachments are stored in ContentDocuments
		ContentDocumentLink docLink = [ SELECT ContentDocumentId
				FROM ContentDocumentLink
				WHERE LinkedEntityId =: recId order by Id desc Limit 1];
		Speaker__c speaker = [SELECT Id FROM Speaker__c WHERE Id = :recId];
		if (docLink != null){

			//ContentVersion Id uniquely identifies the attachment
			ContentVersion ver = [SELECT Id FROM ContentVersion Where ContentDocumentId = :docLink.ContentDocumentId];
			//Update the Picture_Path field with the url of the image
			speaker.Picture_Path__c = '/sfc/servlet.shepherd/version/download/' + ver.Id;
		}
		upsert speaker;
	}

}