public class MaintenanceRequestHelper
{
	public static final String MAINTENANCE_REQUEST_TYPE_REPAIR = 'Repair';
	public static final String MAINTENANCE_REQUEST_TYPE_OTHER = 'Other';
	public static final String MAINTENANCE_REQUEST_TYPE_ROUTINE = 'Routine Maintenance';
	public static final String MAINTENANCE_REQUEST_STATUS_CLOSED = 'Closed';
	public static final String MAINTENANCE_REQUEST_STATUS_NEW = 'New';
	public static void updateWorkOrders(Map<Id, Case> closedCases)
	{
		List<Work_Part__c> workParts = [SELECT Id, Equipment__r.Maintenance_Cycle__c, Maintenance_Request__c FROM Work_Part__c WHERE Maintenance_Request__c IN : closedCases.keySet()];
		Map<Id, List<Work_Part__c>> casesWorkParts = Utils.getParentIdToChildrenMapping(workParts, 'Maintenance_Request__c');

		Decimal shortestCycle;
		List<Work_Part__c> allWorkParts = workParts;
		List<Case> newCases = new List<Case>();
		for (Case c : closedCases.values())
		{
			List<Work_Part__c> relatedWorkParts = new List<Work_Part__c>();
			for (Work_Part__c workpart: WorkParts)
			{
				if (c.Id == workpart.Maintenance_Request__c)
				{
					relatedWorkParts.add(workpart);
				}
			}
			shortestCycle = getMaintenanceCycle(relatedWorkParts);
			Case newCase = new Case();
			newCase.Vehicle__c = c.Vehicle__c;
			newCase.Type = MAINTENANCE_REQUEST_TYPE_ROUTINE;
			newCase.Status = MAINTENANCE_REQUEST_STATUS_NEW;
			newCase.Reason = c.Reason;
			newCase.Subject = String.isBlank(c.Subject) ? 'Routine Maintenance Request' :
							  c.Subject;
			newCase.Date_Reported__c = Date.today();
			newCase.Date_Due__c = Date.today().addDays( Integer.valueOf(shortestCycle));
			newCase.Equipment__c = c.Equipment__c;
			newCase.Old_Case__c = c.Id;
			newCases.add(newCase);

		}
		if (!newCases.isEmpty())
		{
			insert newCases;
			updateRelatedWorkOrders(newCases, allWorkParts);
		}
	}

	public static Decimal getMaintenanceCycle(List<Work_Part__c> workParts){
		Decimal maintenanceCycle = 0.0;
		List<Decimal> cycleList = new List<Decimal>();
		if (!workParts.isEmpty())
		{
			for (Work_Part__c part: workParts)
			{
				cycleList.add(part.Equipment__r.Maintenance_Cycle__c);
				cycleList.sort();
			}
		}

		if (!cycleList.isEmpty())
		{
			maintenanceCycle = cycleList.get(0);
		}
		return maintenanceCycle;
	}

	private static void updateRelatedWorkOrders(List<Case> cases, List<Work_Part__c> workParts){
		Map<Id, Id> oldToNewCaseMap = new Map<Id, Id>();
		for (Case singleCase : cases){
			oldToNewCaseMap.put(singleCase.Old_Case__c,singleCase.Id);
		}

		if (workParts != null){
			for(Work_Part__c singleWorkPart : workParts){
			//	Id newCaseId = oldToNewCaseMap.get(singleWorkPart.Maintenance_Request__c);
			//	singleWorkPart.Maintenance_Request__c = newCaseId;
			}
		}

		if (workParts != null && !workParts.isEmpty()){
			update workParts;
		}
	}

}