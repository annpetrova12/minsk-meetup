public class RandomContactFactory {

    public static List<Contact> generateRandomContacts(Integer numberOfContacts, String lastName){
        List<Contact> newList = new List<Contact>();
        for(Integer i=0; i<numberOfContacts; i++){
            newList.add(new Contact(LastName = lastname, FirstName = 'x'+i));
        }
        return newList;
    }
}