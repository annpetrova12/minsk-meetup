@isTest
public class AddressSyncFormControllerTest
{
	@testSetup
	static void setup()
	{
		Account acc = new Account(Name = 'Test');
		insert acc;
	}

	@isTest
	static void test_getRecord()
	{
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Test.startTest();
			SObject record = AddressSyncFormCmpController.getRecord(acc.Id);
		Test.stopTest();

		System.assertEquals(acc.getSObjectType(), record.getSObjectType());
	}

	@isTest
	static void test_updateRecord()
	{
		Account acc = [SELECT Id, Name FROM Account LIMIT 1];
		acc.Name = 'New name';

		Test.startTest();
			AddressSyncFormCmpController.saveAddress(acc);
		Test.stopTest();

		System.assertEquals([SELECT Name FROM Account LIMIT 1].Name, acc.Name);
	}
}