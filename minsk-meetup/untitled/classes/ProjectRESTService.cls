@RestResource(urlMapping = '/project/*')
global class ProjectRESTService {
    //Implement service logic here
	@HttpPost
	global static String postProjectData(String ProjectRef, String ProjectName, String OpportunityId, Date StartDate, Date EndDate, Double Amount, String Status)
	{
		Savepoint sp = Database.setSavepoint();
		RestRequest request = RestContext.request;
		RestResponse response = RestContext.response;
	//	ProjectWrapper projectWrapper = (ProjectWrapper) JSON.deserialize(request.requestBody.toString(), ProjectWrapper.class);

		List<Project__c> existProjects = [SELECT Billable_Amount__c,End_Date__c,Id,Name,Opportunity__c,ProjectRef__c,Start_Date__c,Status__c FROM Project__c WHERE ProjectRef__c =: ProjectRef];
		Project__c project;
		if (existProjects.isEmpty())
		{
			project = new Project__c();
			project.Opportunity__c = OpportunityId;
		}
		else
		{
			project = existProjects.get(0);
		}

		project.Name = ProjectName;
		project.ProjectRef__c = ProjectRef;
		project.Start_Date__c = StartDate;
		project.End_Date__c = EndDate;
		project.Billable_Amount__c = Amount;
		project.Status__c = Status;

		String message;
		try
		{
			upsert project;
			Opportunity opportunity = [SELECT Id, DeliveryInstallationStatus__c FROM Opportunity WHERE Id = :OpportunityId];
			opportunity.DeliveryInstallationStatus__c = 'In progress';
			update opportunity;

			message = 'OK';
		}
		catch(DmlException ex)
		{
			Database.rollback(sp);
			message = ex.getMessage();
		}

		return message;
	}
/*
	public class ProjectWrapper
	{
		public String projectRef {get; set;}
		public String projectName {get; set;}
		public String opportunityId {get; set;}
		public String startDate {get; set;}
		public String endDate {get; set;}
		public Double amount {get; set;}
		public String status {get; set;}
	}*/
}