@isTest
public class NumberOfContactsOnAccount_Test 
{
    @isTest
    public static void testInsert()
    {
        List<Contact> contactList = new List<Contact>();
        List<Account> accounts = new List<Account>();
        
        for(Integer i=0; i<6; i++)
        {
            accounts.add(new Account(Name='TestAcc'+i));
        }        
        insert accounts;
        
        Test.startTest();

        for(Integer i=0; i<100; i++)
        {
            if(i<5)
            {
                contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(0).id));
            }
            else if(i>=6 && i<26){
                contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(1).id));
            }
            else if(i>=27 && i< 37){
                 contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(2).id));
            }
             else if(i>=38 && i< 68){
                 contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(3).id));
            }
             else if(i>=68 && i< 80){
                 contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(4).id));
            }
             else if(i>=80 && i< 100){
                 contactList.add(new Contact(LastName='TestContact'+i, AccountId=accounts.get(5).id));
            }
        }
        insert contactList;
        Test.stopTest();
        
        List<Account> testAccounts = [select Name, Number_of_Contacts__c from Account];
        for(Account acc: testAccounts)
        {
            if(acc.id==accounts.get(0).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,5);
            }
            else if(acc.id==accounts.get(1).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,20);
            }
            else if(acc.id==accounts.get(2).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,10);
            }
            else if(acc.id==accounts.get(3).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,30);
            }
            else if(acc.id==accounts.get(4).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,12);
            }
            else if(acc.id==accounts.get(5).id)
            {
                System.assertEquals(acc.Number_of_Contacts__c,20);
            }
        }
    }
}