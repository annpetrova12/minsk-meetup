public class AddPrimaryContact implements Queueable
{
    Contact cont;
    String stateAbbr;
	public AddPrimaryContact(Contact cont, String stateAbbr)
    {
        this.cont = cont;
        this.stateAbbr = stateAbbr;
    }
    
    public void execute(QueueableContext context)
    {
        List<Account> accs = [SELECT Id FROM Account WHERE BillingState =: this.stateAbbr limit 200];
        List<Contact> newConts = new List<Contact>();
        for (Account acc : accs)
        {
            newConts.add(cont.clone());
        }
        insert newConts;
    }
}