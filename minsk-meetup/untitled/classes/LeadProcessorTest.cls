@isTest
public class LeadProcessorTest 
{
	@isTest
    static void testBatch()
    {
        List<Lead> leads = new List<Lead>();
        for (Integer i=0; i<200; i++)
        {
            leads.add(new Lead(LastName = 'Test', Company = 'Test', LeadSource = 'Dreamforce'));
        }
        insert leads;
        Test.startTest();
        Database.executeBatch(new LeadProcessor());
        Test.stopTest();
    }
}