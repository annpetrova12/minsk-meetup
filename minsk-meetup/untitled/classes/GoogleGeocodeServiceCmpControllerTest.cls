@isTest
public class GoogleGeocodeServiceCmpControllerTest
{
	@isTest
	private static void testGoogleAddressApi()
	{
		Test.setMock(HttpCalloutMock.class, new GoogleGeocodeServiceMock());
		
		Test.startTest();
		
		Google_Maps_API_Settings__mdt settings = GoogleGeocodeServiceCmpController.getConnectionSettings('Default_Settings');
		String body = GoogleGeocodeServiceCmpController.searchAddress('Convene 101', settings);
		
		Test.stopTest();
		
		System.assert(!String.isEmpty(body));
	}
}