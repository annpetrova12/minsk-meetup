@isTest
private class ProjectRESTServiceTest {

	@isTest
	static void testSuccess()
	{
		Account acct = new Account(Name='Test Account');
		insert acct;
		LIST<Opportunity> opps = new LIST<Opportunity>();
		opps.add(new Opportunity(
				Name = 'Test Opportunity',
				AccountId = acct.id,
				CloseDate = System.Today(),
				Amount = 12480.00,
				Type = 'New Project',
				StageName='Qualification'));
		insert opps;

		Test.startTest();
			String strResponse = ProjectRESTService.postProjectData('testing1104', 'testing1104',
					string.valueOf(opps[0].Id), opps[0].CloseDate, opps[0].CloseDate, double.valueOf(opps[0].Amount), 'Running');
			System.assertEquals('OK', strResponse);
		Test.stopTest();

		Opportunity opp = [select Id, DeliveryInstallationStatus__c from Opportunity where Id = :opps[0].Id LIMIT 1][0];
		System.assertEquals('In progress', opp.DeliveryInstallationStatus__c, 'Success');
	}

	@isTest
	static void testFailure()
	{
		Account acct = new Account(Name='Test Account');
		insert acct;

		LIST<Opportunity> opps = new LIST<Opportunity>();
		opps.add(new Opportunity(
				Name = 'Test Opportunity',
				AccountId = acct.id,
				CloseDate = System.Today(),
				Amount = 12480.00,
				Type = 'New Project',
				StageName='Qualification'));
		insert opps;

		Test.startTest();
		String strResponse = ProjectRESTService.postProjectData('testing1104', 'testing1104',
				string.valueOf(opps[0].Id), opps[0].CloseDate, opps[0].CloseDate.addDays(-1), double.valueOf(opps[0].Amount), 'Running');
		system.assertNotEquals('OK', strResponse, 'Fail');
		Test.stopTest();

	}
}