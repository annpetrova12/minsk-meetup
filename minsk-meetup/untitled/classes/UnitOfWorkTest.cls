@isTest
public class UnitOfWorkTest {
   @isTest
    public static void challengeComplete(){
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
    new Schema.SObjectType[] {
        Account.SObjectType,
        Contact.SObjectType,
        Note.SObjectType
    });
        Account acc = new Account();
        acc.Name = ' : Product : ';
        insert acc;
        for(Integer i=0; i<99; i++) {
      Account product = new Account();
      product.Name = ' : Product : ' + i;
      uow.registerNew(product);
        }
        for(Integer i=0; i<500; i++) {
      Contact product = new Contact(LastName = ' : Product : ' + i);
      uow.registerNew(product);
            Note product1 = new Note(ParentId = acc.Id, Title='job'+i);
      uow.registerNew(product1);
        }
            uow.commitWork();
        System.assertEquals(100, [Select Id from Account].size());
        System.assertEquals(500, [Select Id from Contact].size());
        System.assertEquals(500, [Select Id from Note].size());
    }
}