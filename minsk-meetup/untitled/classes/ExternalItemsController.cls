public with sharing class ExternalItemsController
{   
    @AuraEnabled
    public static Integer getAmountOfExternalItems(String recordId)
    {
        return [SELECT COUNT() FROM External_Item__c WHERE ParentID__c =: recordId];
    }
    
    @AuraEnabled
    public static List<External_Item__c> getExternalItems(Integer amountOfVisible, Integer numberOfPage, String recordId)
    {
        Integer amount = Integer.valueOf(amountOfVisible);
        Integer currentPosition = 2 * (Integer.valueOf(numberOfPage) - 1);
        List<External_Item__c> externalItems = [SELECT
                                                	Id,
                                                	Name__c,
                                                	Text__c,
                                                	ParentID__c,
                                                	CreatedDate
                                                FROM
                                                	External_Item__c
                                                WHERE
                                                	ParentID__c =: recordId
                                                LIMIT
                                                	:amount
                                                OFFSET
                                                	:currentPosition];
    
        return externalItems;
    }
    
    @AuraEnabled
    public static void deleteRecord(External_Item__c record)
    {
        delete record;
    }
    
    @AuraEnabled
    public static Boolean isUpdateable(String recordId)
    {
        Boolean description = ((Id)recordId).getSobjectType().getDescribe().isUpdateable();
        return description;
    }
}