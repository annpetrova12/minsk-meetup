@isTest
public class ProjectCalloutServiceTest {
  //Implement mock callout tests here
	@testSetup
	static void testSetup()
	{
		ServiceTokens__c serviceToken = new ServiceTokens__c(Token__c = 'test1223', Name = 'ProjectServiceToken');
		insert serviceToken;
	}

	@isTest
	static void test_successProjectRestCallout()
	{
		Opportunity opp = new Opportunity(Name = 'Test', StageName = 'Closed Won',
				Type = 'New Project', CloseDate = Date.today(), Discount_Percent__c = 10);
		insert opp;
		opp = [SELECT Id, StageName FROM Opportunity WHERE Id =: opp.Id LIMIT 1];
		Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMock());
		Test.startTest();
			ProjectCalloutService.postOpportunityToPMS(new List<Id>{opp.Id});
		Test.stopTest();

	//	System.assertEquals('Submitted Project', [SELECT StageName FROM Opportunity WHERE Id =: opp.Id LIMIT 1].StageName, 'Bad success request');
	}

	@isTest
	static void test_failureProjectRestCallout()
	{
		Opportunity opp = new Opportunity(Name = 'Test', StageName = 'Closed Won',
				Type = 'New Project', CloseDate = Date.today(), Discount_Percent__c = 10);
		insert opp;
		opp = [SELECT Id, StageName FROM Opportunity WHERE Id =: opp.Id LIMIT 1];
		Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMockFailure());
		Test.startTest();
			ProjectCalloutService.postOpportunityToPMS(new List<Id>{opp.Id});
		Test.stopTest();

	//	System.assertEquals('Resubmit Project', [SELECT StageName FROM Opportunity WHERE Id =: opp.Id LIMIT 1].StageName,'Bad success request');
	}
}