@isTest
public class AccountManagerTest 
{
     @isTest static void testGetCaseById() {
        Account acc = new Account(Name = 'test');
         insert acc;
        Id recordId = acc.Id;
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri = 'https://yourInstance.salesforce.com/services/apexrest/Accounts/' + recordId + '/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        Account thisAcc = AccountManager.getAccount();
        // Verify results
        System.assert(thisAcc != null);
    }
}