/**
 * Created by anna.petrova on 6/28/2018.
 */
@isTest
public class WarehouseSchedule_Test
{
	public static String CRON_EXP = '0 0 0 15 3 ? 2019';

	@isTest
	static void testWarehouseSchedule()
	{
		Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
		Test.startTest();
			String jobId = System.schedule('ScheduledApexTest', CRON_EXP,
					new WarehouseSyncSchedule());
		Test.stopTest();
	}
}