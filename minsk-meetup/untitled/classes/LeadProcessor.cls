public class LeadProcessor implements Database.Batchable<sObject>
{
	public Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator('SELECT Id, LeadSource FROM Lead');
    }
    
    public void execute (Database.BatchableContext bc, List<Lead> leads)
    {
        List<Lead> leadsToUpdate = new List<Lead>();
        for (Lead le :leads)
        {
            if (le.LeadSource == 'Dreamforce')
            {
                leadsToUpdate.add(le);
            }
        }
        update leadsToUpdate;
    }
    
    public void finish(Database.BatchableContext bc)
    {
        
    }
}