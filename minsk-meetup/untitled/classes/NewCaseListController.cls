/**
 * Created by anna.petrova on 8/10/2018.
 */
public class NewCaseListController
{
	public List<Case> getNewCases()
	{
		return [SELECT Id, CaseNumber FROM Case WHERE Status = 'New'];
	}
}