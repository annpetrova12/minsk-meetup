public class Utils
{
	///////////////////////////////////////////////
	public static void logHttpRequest(HttpRequest request)
	{
		System.debug('REQUEST');
		System.debug('Endpoint: ' + request.getEndpoint());
		System.debug('Method: ' + request.getMethod());
		System.debug('Body: ' + request.getBody());
	}

	public static void logHttpResponse(HttpResponse response)
	{
		System.debug('RESPONSE');
		System.debug('Status Code: ' + response.getStatus());
		System.debug('Body: ' + response.getBody());
	}
	///////////////////////////////////////////////////
	private static Map<String, Id> recordTypesMap
	{
		get
		{
			if (recordTypesMap == null )
			{
				recordTypesMap = new Map<String, Id>();
				for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true])
				{
					recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
					System.debug(aRecordType.SobjectType + ':' + aRecordType.DeveloperName);
				}
			}
			return recordTypesMap;
		}
		set;
	}

	public static Map<Id, List<sObject>> getParentIdToChildrenMapping(List<sObject> childrenRecords, String fieldName)
	{
		Map<Id, List<sObject>> mappingResult = new Map<Id, List<sObject>>();
		for (sObject sObjectRecord : childrenRecords)
		{
			Id parentId = (Id)sObjectRecord.get(fieldName);
			if (parentId != null)
			{
				if(mappingResult.get(parentId) == null)
				{
					mappingResult.put(parentId, new List<sObject>());
				}
				mappingResult.get(parentId).add(sObjectRecord);
			}
		}
		return mappingResult;
	}

	public static list<SelectOption> getPicklistValues(SObject obj, String fld) {
		List<SelectOption> options = new list<SelectOption>();
		Schema.sObjectType objType = obj.getSObjectType();
		Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
		map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();

		List<Schema.PicklistEntry> values =
				fieldMap.get(fld).getDescribe().getPickListValues();

		for (Schema.PicklistEntry a : values) {
			options.add(new SelectOption(a.getLabel(), a.getValue()));
		}
		return options;
	}

	public static Map<String, Id> getRecordTypesMap()
	{
		return recordTypesMap;
	}

	public static Id getRecordTypeId(String recTypeName)
	{
		if (recordTypesMap.containsKey(recTypeName))
		{
			return recordTypesMap.get(recTypeName);
		}
		else
		{
			throw new MyException('No record type found for: ' + recTypeName);
		}
	}

	private class MyException extends Exception{}
}