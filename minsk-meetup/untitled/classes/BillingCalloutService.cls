public class BillingCalloutService {
    //Implement business and callout logic methods here
	@future(callout = true)
	public static void callBillingService(String projectRef, Decimal billAmount, Id projId)
	{
		//List<Project__c> projects = [SELECT Id, ProjectRef__c, Billable_Amount__c FROM Project__c WHERE Id IN : billableProjects];
		ServiceCredentials__c sc = ServiceCredentials__c.getValues('BillingServiceCredential');

		Project__c pr = new Project__c(Id = projId, ProjectRef__c = projectRef);
		BillingServiceProxy.project billProject = new BillingServiceProxy.project();
		billProject.projectRef = projectRef;
		billProject.billAmount = billAmount;
		billProject.password = sc.Password__c;
		billProject.username = sc.Username__c;
		String response = new BillingServiceProxy.InvoicesPortSoap11().billProject(billProject);

		if (response.equals('ok'))
		{
			pr.Status__c = 'Billed';
			update pr;
		}
		else
		{
			System.debug('Failed response: ' + response);
		}
	}
}