public with sharing class GoogleGeocodeServiceCmpController
{
	public static final String HTTP_STATUS_CODE_OK = 'OK';
	
	@AuraEnabled
	public static Google_Maps_API_Settings__mdt getConnectionSettings(String connectionSettingsDeveloperName)
	{
		List<Google_Maps_API_Settings__mdt> connectionSettings = [
			SELECT Google_Maps_API_Key__c, Geocode_Search_Endpoint__c
			FROM Google_Maps_API_Settings__mdt
			WHERE DeveloperName = :connectionSettingsDeveloperName
			LIMIT 1
		];

		if (!connectionSettings.isEmpty())
		{
			return connectionSettings[0];
		}

		return null;
	}

	@AuraEnabled
	public static String searchAddress(String searchLine, Google_Maps_API_Settings__mdt connectionSettings)
	{
		String requestURL = connectionSettings.Geocode_Search_Endpoint__c
			+ '?key=' + connectionSettings.Google_Maps_API_Key__c
			+ '&address=' + EncodingUtil.urlEncode(searchLine, 'UTF-8');

		Http http = new Http();

		HttpRequest searchRequest = new HttpRequest();
		searchRequest.setMethod('GET');
		searchRequest.setEndpoint(requestURL);

		HttpResponse searchResponse = http.send(searchRequest);
		
		if (searchResponse.getStatus() == HTTP_STATUS_CODE_OK)
		{
			return searchResponse.getBody();
		}

		Utils.logHttpRequest(searchRequest);
		Utils.logHttpResponse(searchResponse);

		throw new AuraHandledException(Label.Google_Geocode_Service_Http_Request_Failed);
	}
}