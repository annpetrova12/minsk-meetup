@isTest
public class TestVerifyDate {
   @isTest private static void checkdatestest(){
        date myDate = date.newInstance(1987, 12, 17);
       Date date2 = mydate.addDays(29);
        System.assertEquals(date2, VerifyDate.CheckDates(myDate, date2));
       date2 = date2.addDays(29);
        System.assertEquals(date2, VerifyDate.CheckDates(myDate, date2));
    }
    
     @isTest private static void DateWithin30Daystest(){
        Date date2 = Date.newInstance(1960, 2, 17);
        date myDate = date.newInstance(1987, 12, 17);
        System.assertEquals(false, VerifyDate.DateWithin30Days(myDate, date2));
        System.assertEquals(false, VerifyDate.DateWithin30Days(myDate, myDate));
        System.assertEquals(true, VerifyDate.DateWithin30Days(myDate, myDate));
    }
    
     @isTest private static void SetEndOfMonthtest(){
        Date date2 = Date.newInstance(1960, 2, 17);
        date myDate = date.newInstance(1987, 12, 17);
        System.assertEquals(date2, VerifyDate.SetEndOfMonthDate(myDate));
    }
}