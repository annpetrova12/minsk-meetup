public class LeadComponentController {
    @AuraEnabled
    public static String saveLeadRecord(String leadRecordJSON){
        Lead newLeadRecord = new Lead();
        
        try{
            newLeadRecord = (Lead)JSON.deserialize(leadRecordJSON, Lead.class);
            
            insert newLeadrecord;
        }
        catch(Exception ex)
        {
            throw new System.AuraHandledException(ex.getMessage()); 
        }
        return newLeadRecord.Id;
    }
     @AuraEnabled
    public static Lead getLeadName(String recordId){
        
        Lead newLeadRecord = [select FirstName, LastName from Lead where id=:recordId];
        return newLeadRecord;
    }
}