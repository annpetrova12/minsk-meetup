/**
 * Created by anna.petrova on 6/28/2018.
 */
@isTest
public class MaintenanceRequestTrigger_Test
{
	private static final Integer NUMBER_OF_CASES = 400;
	private static final Date SPECIFIED_DATE = Date.newInstance(2018, 6, 25);
	@testSetup
	static void testSetup()
	{
		Vehicle__c vehicle = new Vehicle__c(Name = 'Test vehicle');
		insert vehicle;

		List<Product2> equipments = new List<Product2>
		{
			new Product2(Name = 'Test Equipment', Maintenance_Cycle__c = 10,
					Cost__c = 100,	Current_Inventory__c = 10,	Lifespan_Months__c = 10,
					Replacement_Part__c = true,	Warehouse_SKU__c = 'abc'),
			new Product2(Name = 'Test Equipment', Maintenance_Cycle__c = 0,
					Cost__c = 0,	Current_Inventory__c = 10,	Lifespan_Months__c = 10,
					Replacement_Part__c = true,	Warehouse_SKU__c = 'abd')
		};
		insert equipments;

		List<Case> cases = new List<Case>();
		for (Integer i = 0; i < NUMBER_OF_CASES / 4; i++)
		{
			cases.add(new Case(Subject = 'Test', Status = MaintenanceRequestHelper.MAINTENANCE_REQUEST_STATUS_NEW,
					Type = MaintenanceRequestHelper.MAINTENANCE_REQUEST_TYPE_REPAIR, Date_Reported__c = SPECIFIED_DATE,
					Date_Due__c = SPECIFIED_DATE.addDays(1), Equipment__c = equipments.get(0).Id,
							Reason = 'Test reason', Vehicle__c = vehicle.Id
			));
		}
		for (Integer i = NUMBER_OF_CASES / 4; i < NUMBER_OF_CASES / 2; i++)
		{
			cases.add(new Case(Subject = 'Test', Status = MaintenanceRequestHelper.MAINTENANCE_REQUEST_STATUS_NEW,
					Type = MaintenanceRequestHelper.MAINTENANCE_REQUEST_TYPE_ROUTINE, Date_Reported__c = SPECIFIED_DATE,
					Date_Due__c = SPECIFIED_DATE.addDays(1), Equipment__c = equipments.get(1).Id,
					Reason = 'Test reason', Vehicle__c = vehicle.Id
			));
		}
		for (Integer i = NUMBER_OF_CASES / 2; i < NUMBER_OF_CASES; i++)
		{
			cases.add(new Case(Subject = 'Test', Status = MaintenanceRequestHelper.MAINTENANCE_REQUEST_STATUS_NEW,
					Type = MaintenanceRequestHelper.MAINTENANCE_REQUEST_TYPE_OTHER, Date_Reported__c = SPECIFIED_DATE,
					Date_Due__c = SPECIFIED_DATE.addDays(1), Equipment__c = equipments.get(0).Id,
					Reason = 'Test reason', Vehicle__c = vehicle.Id
			));
		}
		insert cases;

		List<Work_Part__c> workparts = new List<Work_Part__c>();
		for (Integer i = 0; i < cases.size() / 2; i++)
		{
			workparts.add(new Work_Part__c(Quantity__c = 10,
					Equipment__c = equipments.get(1).Id, Maintenance_Request__c = cases.get(i).Id));
		}
		insert workparts;
	}

	@isTest
	static void testTriggerWhenMaintenanceRequestClosed_OnUpdate()
	{
		Map<Id, Case> cases = new Map<Id, Case>([SELECT Id FROM Case]);
		System.assertEquals(NUMBER_OF_CASES, cases.size());

		for (Case caseItem : cases.values())
		{
			caseItem.Status = MaintenanceRequestHelper.MAINTENANCE_REQUEST_STATUS_CLOSED;
		}

		Test.startTest();
			update cases.values();
		Test.stopTest();

		System.assertEquals([SELECT COUNT() FROM Case WHERE Id NOT IN :cases.keySet()], NUMBER_OF_CASES * 0.5, 'Invalid number of created cases');
	}
}