@isTest
global with sharing class GoogleGeocodeServiceMock implements HttpCalloutMock
{
	global static final String URI = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyD4pQztTL6ZrVGN8tVBCEirrVJNfplQ2w0&address=';
	
	global HTTPResponse respond(HTTPRequest req) {
		System.assert(req.getEndpoint().contains(URI));
		System.assertEquals('GET', req.getMethod());
		
		HttpResponse response = new HttpResponse();
		response.setHeader('Content-Type', 'application/json');
		response.setStatus('OK');
		response.setBody('{ "results" : [ { "address_components" : [ { "long_name" : "101", "short_name" : "101", "types"' +
			' : [ "street_number" ] }, { "long_name" : "Park Avenue", "short_name" : "Park Ave", "types" : [ "route" ] },' +
			' { "long_name" : "Manhattan", "short_name" : "Manhattan", "types" : [ "political", "sublocality", "sublocality_level_1" ]' +
			' }, { "long_name" : "New York", "short_name" : "New York", "types" : [ "locality", "political" ] },' +
			' { "long_name" : "New York County", "short_name" : "New York County", "types" : [ "administrative_area_level_2",' +
			' "political" ] }, { "long_name" : "New York", "short_name" : "NY", "types" : [ "administrative_area_level_1", "political" ] },' +
			' { "long_name" : "United States", "short_name" : "US", "types" : [ "country", "political" ] }, { "long_name" : "10178",' +
			' "short_name" : "10178", "types" : [ "postal_code" ] } ], "formatted_address" : "101 Park Ave, New York, NY 10178, USA", ' +
			'"geometry" : { "location" : { "lat" : 40.7511138, "lng" : -73.9780523 }, "location_type" : "ROOFTOP", "viewport" : ' +
			'{ "northeast" : { "lat" : 40.75246278029149, "lng" : -73.97670331970851 }, "southwest" : { "lat" : 40.7497648197085,' +
			' "lng" : -73.97940128029151 } } }, "place_id" : "ChIJ41TVaAFZwokRXEnJY4vp4-I", "types" : [ "establishment", ' +
			'"point_of_interest" ] } ], "status" : "OK" }');
		response.setStatusCode(200);
		return response;
	}
}