public class AccountProcessor 
{
    @future
    public static void countContacts(List<Id> accIds)
    {
        List<Account> accs = [select id, Number_of_Contacts__c, (select id from Contacts) from Account WHERE Id IN :accIds];
        for (Account acc : accs)
        {
            acc.Number_of_Contacts__c = acc.Contacts.size();
        }
        update accs;        
    }
}