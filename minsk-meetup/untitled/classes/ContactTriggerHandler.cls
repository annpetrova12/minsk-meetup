/**
 * Created by user on 10.12.2016.
 */
global class ContactTriggerHandler{

    global static void calculateContacts(Set<Id> accounts) {

        List<Account> accountList = [
                select Id, Number_of_Contacts__c, (select id From Account.Contacts)
                from Account
                where Id IN:accounts
        ];
        for (Account acc: accountList) {
            acc.Number_of_Contacts__c = acc.Contacts.size();
        }
        update accountList;
    }
}