public class CampingListController 
{
    @AuraEnabled
    public static List<Camping_Item__c> getItems()
    {
        String[] fieldsToCheck = new String[] {'Id', 'Name', 'Price__', 'Quantity__c', 'Packed'};
            
            Map<String, Schema.SObjectField> fieldDescribeTokens = 
            	Schema.SObjectType.Camping_Item__c.fields.getMap();
        
        for (String field : fieldsToCheck)
        {
            if (! fieldDescribeTokens.get(field).getDescribe().isAccessible())
            {
                throw new System.NoAccessException();
            }
        }
        List<Camping_Item__c> campingItems = [SELECT Name, Price__c, Quantity__c, Packed__c FROM Camping_Item__c];
        return campingItems;
    }
    
    @AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c item)
    {
        
        upsert item;
        return item;
    }
}