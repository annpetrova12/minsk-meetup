public class AddressSyncFormCmpController
{
	@AuraEnabled
	public static SObject getRecord(String recordId)
	{
		String sObjectType = ((Id)recordId).getSObjectType().getDescribe().getName();
		System.debug(LoggingLevel.ERROR, 'type: ' + sObjectType);
		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectType);
		SObject record = targetType.newSObject(recordId);
		System.debug(LoggingLevel.ERROR, 'record: ' + record);
		return record;
	}

	@AuraEnabled
	public static void saveAddress(SObject record)
	{
		try
		{
			update record;
		}
		catch(AuraHandledException ex)
		{
			System.debug(LoggingLevel.ERROR, 'Error: ' + ex.getMessage());
		}
	}
}